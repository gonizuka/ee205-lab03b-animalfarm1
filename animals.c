///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include<stdbool.h>
#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
   switch(color){
      case BLACK:
         return "Black";
         break;
      case WHITE:
         return "White";
         break;
      case RED:
         return "Red";
         break;
      case BLUE:
         return "Blue";
         break;
      case GREEN:
         return "Green";
         break;
      case PINK:
         return "Pink";
         break;
      default:
         return NULL;
         break;
   }
}

char* genderName (enum Gender gender){
   switch(gender){
      case MALE:
         return "Male";
         break;
      case FEMALE:
         return "Female";
         break;
      default:
         return NULL;
         break;
   }
}

char* breedName (enum CatBreed breed) {
   switch(breed){
      case MAIN_COON:
         return "Main Coon";
         break;
      case MANX:
         return "Manx";
         break;
      case SHORTHAIR:
         return "Shorthair";
         break;
      case PERSIAN:
         return "Persian";
         break;
      case SPHYNX:
         return "Sphynx";
         break;
      default:
         return NULL;
         break;

   }
}

char* yesno(bool isFixed){
   if(true){
      return "yes";
   }
   else if(false){
      return "no";
   }
   else{
      return NULL;
   }
}
